package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        boolean running = true;
        while (running) {
            System.out.printf("Let's play round %d\n", roundCounter);
            oneRound();
            String answer = readInput("Do you wish to continue playing? (y/n)?");
            if (answer.equals("n")) {
                running = false;
                System.out.println("Bye bye :)");
            } 
        }   
    }

    private void oneRound() {
        String humanChoice = validateChoice();
        String computerChoice = computerChoice();
        String result = "Human chose " + humanChoice + ", computer chose " + computerChoice + ".";     

        if (humanChoice.equals(computerChoice)) {
            result += " It's a tie!";
        } else if (humanChoice.equals("rock")) {
            boolean computerDidWin = computerChoice.equals("paper");
            setScore(computerDidWin);
            result += computerDidWin ? " Computer wins!" : " Human wins!" ;
        } else if (humanChoice.equals("paper")) {
            boolean computerDidWin = computerChoice.equals("scissors");
            setScore(computerDidWin);
            result += computerChoice.equals("scissors") ? " Computer wins!" : " Human wins!" ;
        } else if (humanChoice.equals("scissors")) {
            boolean computerDidWin = computerChoice.equals("rock");
            setScore(computerDidWin);
            result += computerChoice.equals("rock") ? " Computer wins!" : " Human wins!" ;
        }
        roundCounter++;
        System.out.println(result);
        System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    }

    private String computerChoice() {
        double randomNumber = Math.random();
        double randomMult = (randomNumber * 3);
        int listIndex = (int) randomMult; 
        String computerChoice = rpsChoices.get(listIndex);
        return computerChoice;
    }

    private String validateChoice() {
        String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
        if (humanChoice.equals("rock") || humanChoice.equals("paper") || humanChoice.equals("scissors")) {
            return humanChoice;
        }
        else {
            System.out.println("I do not understand " + humanChoice + ". Could you try again?");
            return validateChoice();
        }    
    }

    private void setScore(boolean computerDidWin) {
        if (computerDidWin) 
            computerScore++;
        else 
            humanScore++;
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
